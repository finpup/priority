const webpack = require('webpack');
const Server = require('webpack-dev-server');
const config = require('./webpack.config');

new Server(webpack(config), {
    headers: { 'Access-Control-Allow-Origin': '*' },
    contentBase: '.',
    publicPath: '.',
    hot: false,
    historyApiFallback: true,
}).listen(8000, 'localhost', (err, result) => {
    if (err) {
        console.log(err, result);
    } else {
        console.log(' Listening on port 8000');
    }
});