import React from 'react';
import { render } from 'react-dom';
import Priority from "./Priority.jsx";


document.addEventListener("DOMContentLoaded", () => {
   render(
       <Priority/>,
       document.getElementById('app-container')
   )
});
